Ethereum Wallet Sample
==================================

Sample ethereum account functions
- create wallet
- get balance 
- transfer ether

Specs
- Node 8.11.1

Getting Started
---------------

```sh
# clone it
cd ethereum
# Install dependencies
npm install
# Start project
npm start

# Start production server:
PORT=8080 npm start
```

Test 
---------------

```sh
# Run unit test cases with coverage
npm test
```
Docs 
---------------

```sh
# Generate api docs
npm run docs
```
