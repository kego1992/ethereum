import Web3 from 'web3';
import config from '../config.json';

const web3 = new Web3(
    new Web3.providers.HttpProvider(config.faucetProvider)
);

export const newWallet = Promise.resolve(web3.eth.accounts.create())

export const balanceCheck = (address) =>
    web3.eth.getBalance(address)
    .then(balance => Promise.resolve({
        "balance": web3.utils.fromWei(balance, 'ether')
    }))
    .catch(error => Promise.reject(error))

export const balanceTransfer = (privateKey, destination, amount) =>
    web3.eth.accounts.signTransaction({
        to: destination,
        value: web3.utils.toWei(amount.toString(), 'ether'),
        gas: 2000000
    }, privateKey)
    .then(({ rawTransaction }) => web3.eth.sendSignedTransaction(rawTransaction))
    .catch(error => Promise.reject(error))