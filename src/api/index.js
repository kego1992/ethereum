import { Router } from 'express';
import { createWallet, getBalance, transaction} from './account';


export default () => {
    let api = Router();

    api.get('/createWallet', createWallet);
    api.get('/getBalance/:address', getBalance);
    api.post('/transaction', transaction);

    return api;
}