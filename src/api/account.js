import Web3 from 'web3';
import { newWallet, balanceCheck, balanceTransfer } from '../lib/accountManager';


const web3 = new Web3(
    new Web3.providers.HttpProvider('https://rinkeby.infura.io/')
);

/**
 * @api {get} /createWallet Generate new ethereum wallet address and private key
 * @apiName createWallet
 * @apiGroup Accounts
 *
 * @apiSuccess {String} privateKey Private key of the wallet.
 * @apiSuccess {String} address  Address of the wallet.
 */

export const createWallet = ({}, res, next) =>
    newWallet.then(wallet => res.json(wallet))
    .catch(next)


/**
 * @api {get}  /getBalance/:address Get balance of the given ethereum wallet address
 * @apiName getBalance
 * @apiGroup Accounts
 * @apiParam {String} address Ethereum wallet address.
 * @apiSuccess {String} balance  Balance of the wallet address.
 */
export const getBalance = ({ params: { address } }, res, next) =>
    balanceCheck(address)
    .then(balance => res.json(balance))
    .catch(next)

/**
 * @api {post} /transaction Create transaction to transfer ether from one account to another
 * @apiName transaction
 * @apiGroup Accounts
 * @apiParam {String} privateKey PrivateKey of source ethereum wallet address.
 * @apiParam {String} destination Destination ethereum wallet address.
 * @apiParam {String} amount Amount to be transfered in ether.
 * @apiSuccess {String} message  Messge of transaction completion.
 */

export const transaction = ({ body: { privateKey, destination, amount } }, res, next) =>
    balanceTransfer(privateKey, destination, amount)
    .then(transaction => res.json({ message: "transaction completed" }))
    .catch(next)