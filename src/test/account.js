let chai = require('chai');
let chaiHttp = require('chai-http');
process.env.PORT = 7777;
let server = require('../index.js');
let should = chai.should();
let config = require('../config.json')
chai.use(chaiHttp);

describe('/GET createWallet', () => {
    it('it should return and object with the private key and the public ETH address', (done) => {
        chai.request(server)
            .get('/createWallet')
            .end((err, res) => {
            	// console.log(res.body)
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.privateKey.should.be.a('string');
                res.body.address.should.be.a('string');
                done();
            });
    });
});


describe('/GET getBalance', () => {
    it('it should return the balance of an ethereum address (Unit of balance is ether)', (done) => {
        chai.request(server)
            .get(`/getBalance/${config.test.account1.address}`)
            .end((err, res) => {
            	// console.log(res.body)
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.balance.should.be.a('string');
                done();
            });
    });
});

describe('/Post transaction ', () => {
    it('it should creates a transaction to send ETH from one address to another', (done) => {
        chai.request(server)
            .post('/transaction')
            .send({amount: 0.001, destination :config.test.account2.address, privateKey :config.test.account1.privateKey })
            .end((err, res) => {
            	// console.log(res.body)
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.message.should.be.a('string','transaction completed');
                done();
            });
    });
});