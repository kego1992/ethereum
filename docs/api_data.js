define({ "api": [
  {
    "type": "get",
    "url": "/createWallet",
    "title": "Generate new ethereum wallet address and private key",
    "name": "createWallet",
    "group": "Accounts",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "privateKey",
            "description": "<p>Private key of the wallet.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>Address of the wallet.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/account.js",
    "groupTitle": "Accounts"
  },
  {
    "type": "get",
    "url": "/getBalance/:address",
    "title": "Get balance of the given ethereum wallet address",
    "name": "getBalance",
    "group": "Accounts",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>Ethereum wallet address.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "balance",
            "description": "<p>Balance of the wallet address.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/account.js",
    "groupTitle": "Accounts"
  },
  {
    "type": "post",
    "url": "/transaction",
    "title": "Create transaction to transfer ether from one account to another",
    "name": "transaction",
    "group": "Accounts",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "privateKey",
            "description": "<p>PrivateKey of source ethereum wallet address.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "destination",
            "description": "<p>Destination ethereum wallet address.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "amount",
            "description": "<p>Amount to be transfered in ether.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Messge of transaction completion.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/account.js",
    "groupTitle": "Accounts"
  }
] });
