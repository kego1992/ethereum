'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _account = require('./account');

exports.default = function () {
    var api = (0, _express.Router)();

    api.get('/createWallet', _account.createWallet);
    api.get('/getBalance/:address', _account.getBalance);
    api.post('/transaction', _account.transaction);

    return api;
};
//# sourceMappingURL=index.js.map