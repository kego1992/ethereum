'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.transaction = exports.getBalance = exports.createWallet = undefined;

var _web = require('web3');

var _web2 = _interopRequireDefault(_web);

var _accountManager = require('../lib/accountManager');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectDestructuringEmpty(obj) { if (obj == null) throw new TypeError("Cannot destructure undefined"); }

var web3 = new _web2.default(new _web2.default.providers.HttpProvider('https://rinkeby.infura.io/'));

/**
 * @api {get} /createWallet Generate new ethereum wallet address and private key
 * @apiName createWallet
 * @apiGroup Accounts
 *
 * @apiSuccess {String} privateKey Private key of the wallet.
 * @apiSuccess {String} address  Address of the wallet.
 */

var createWallet = exports.createWallet = function createWallet(_ref, res, next) {
    _objectDestructuringEmpty(_ref);

    return _accountManager.newWallet.then(function (wallet) {
        return res.json(wallet);
    }).catch(next);
};

/**
 * @api {get}  /getBalance/:address Get balance of the given ethereum wallet address
 * @apiName getBalance
 * @apiGroup Accounts
 * @apiParam {String} address Ethereum wallet address.
 * @apiSuccess {String} balance  Balance of the wallet address.
 */
var getBalance = exports.getBalance = function getBalance(_ref2, res, next) {
    var address = _ref2.params.address;
    return (0, _accountManager.balanceCheck)(address).then(function (balance) {
        return res.json(balance);
    }).catch(next);
};

/**
 * @api {post} /transaction Create transaction to transfer ether from one account to another
 * @apiName transaction
 * @apiGroup Accounts
 * @apiParam {String} privateKey PrivateKey of source ethereum wallet address.
 * @apiParam {String} destination Destination ethereum wallet address.
 * @apiParam {String} amount Amount to be transfered in ether.
 * @apiSuccess {String} message  Messge of transaction completion.
 */

var transaction = exports.transaction = function transaction(_ref3, res, next) {
    var _ref3$body = _ref3.body,
        privateKey = _ref3$body.privateKey,
        destination = _ref3$body.destination,
        amount = _ref3$body.amount;
    return (0, _accountManager.balanceTransfer)(privateKey, destination, amount).then(function (transaction) {
        return res.json({ message: "transaction completed" });
    }).catch(next);
};
//# sourceMappingURL=account.js.map