'use strict';

var chai = require('chai');
var chaiHttp = require('chai-http');
process.env.PORT = 7777;
var server = require('../index.js');
var should = chai.should();
var config = require('../config.json');
chai.use(chaiHttp);

describe('/GET createWallet', function () {
    it('it should return and object with the private key and the public ETH address', function (done) {
        chai.request(server).get('/createWallet').end(function (err, res) {
            // console.log(res.body)
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.privateKey.should.be.a('string');
            res.body.address.should.be.a('string');
            done();
        });
    });
});

describe('/GET getBalance', function () {
    it('it should return the balance of an ethereum address (Unit of balance is ether)', function (done) {
        chai.request(server).get('/getBalance/' + config.test.account1.address).end(function (err, res) {
            // console.log(res.body)
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.balance.should.be.a('string');
            done();
        });
    });
});

describe('/Post transaction ', function () {
    it('it should creates a transaction to send ETH from one address to another', function (done) {
        chai.request(server).post('/transaction').send({ amount: 0.001, destination: config.test.account2.address, privateKey: config.test.account1.privateKey }).end(function (err, res) {
            // console.log(res.body)
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.message.should.be.a('string', 'transaction completed');
            done();
        });
    });
});
//# sourceMappingURL=account.js.map