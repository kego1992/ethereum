'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.balanceTransfer = exports.balanceCheck = exports.newWallet = undefined;

var _web = require('web3');

var _web2 = _interopRequireDefault(_web);

var _config = require('../config.json');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var web3 = new _web2.default(new _web2.default.providers.HttpProvider(_config2.default.faucetProvider));

var newWallet = exports.newWallet = Promise.resolve(web3.eth.accounts.create());

var balanceCheck = exports.balanceCheck = function balanceCheck(address) {
    return web3.eth.getBalance(address).then(function (balance) {
        return Promise.resolve({
            "balance": web3.utils.fromWei(balance, 'ether')
        });
    }).catch(function (error) {
        return Promise.reject(error);
    });
};

var balanceTransfer = exports.balanceTransfer = function balanceTransfer(privateKey, destination, amount) {
    return web3.eth.accounts.signTransaction({
        to: destination,
        value: web3.utils.toWei(amount.toString(), 'ether'),
        gas: 2000000
    }, privateKey).then(function (_ref) {
        var rawTransaction = _ref.rawTransaction;
        return web3.eth.sendSignedTransaction(rawTransaction);
    }).catch(function (error) {
        return Promise.reject(error);
    });
};
//# sourceMappingURL=accountManager.js.map