# ethereum v1.0.0

Etherem sample application

- [Accounts](#accounts)
	- [Generate new ethereum wallet address and private key](#generate-new-ethereum-wallet-address-and-private-key)
	- [Get balance of the given ethereum wallet address](#get-balance-of-the-given-ethereum-wallet-address)
	- [Create transaction to transfer ether from one account to another](#create-transaction-to-transfer-ether-from-one-account-to-another)
	


# Accounts

## Generate new ethereum wallet address and private key



	GET /createWallet


## Get balance of the given ethereum wallet address



	GET /getBalance/:address


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| address			| String			|  <p>Ethereum wallet address.</p>							|

## Create transaction to transfer ether from one account to another



	POST /transaction


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| privateKey			| String			|  <p>PrivateKey of source ethereum wallet address.</p>							|
| destination			| String			|  <p>Destination ethereum wallet address.</p>							|
| amount			| String			|  <p>Amount to be transfered in ether.</p>							|


